import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"]
})
export class MainComponent implements OnInit, OnDestroy {
  public formGroup: FormGroup;
  public file: File;
  public itemsRef: AngularFireList<any>;
  public items: any[];
  public errorMessage: string = null;
  public showFileName = false;
  private fileName: string;
  private subscriptions = new Subscription();

  @ViewChild("fileEl", { static: false })
  fileInput: ElementRef;

  constructor(
    private fb: FormBuilder,
    private storage: AngularFireStorage,
    private db: AngularFireDatabase,
    private snackBar: MatSnackBar
  ) {}

  public ngOnInit() {
    this.formGroup = this.fb.group({
      fileInput: [null]
    });

    this.itemsRef = this.db.list("images");

    this.subscriptions.add(
      this.itemsRef.valueChanges().subscribe(
        res => {
          this.items = [...res];
        },
        err => {
          this.errorMessage = "Something get wrong...";
          this.snackBar.open(`${this.errorMessage}`, "", {
            duration: 4000
          });
        }
      )
    );
  }

  public onClearFile() {
    this.file = null;
    this.formGroup.reset();
  }

  public onFileChanged(event) {
    this.file = event.target.files[0];
    this.fileName = this.file.name;
  }

  public onSelect() {
    this.fileInput.nativeElement.dispatchEvent(new MouseEvent("click"));
  }

  public onUpload() {
    const ref = this.storage.ref(this.fileName);
    const task = ref.put(this.file);

    this.file = null;
    this.formGroup.reset();

    task
      .then(res => {
        this.subscriptions.add(
          ref.getDownloadURL().subscribe(url => {
            this.itemsRef.push({
              name: this.fileName,
              url
            });
          })
        );
      })
      .catch(err => {
        this.errorMessage = "failed to upload image";

        this.snackBar.open(`${this.errorMessage}`, "", {
          duration: 4000
        });
      });
  }

  public ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
