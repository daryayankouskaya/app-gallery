// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyDSZfKAFR5rmmT_k2eRNO-ejVODK7AfwVQ",
  authDomain: "app-gallery-1ccfd.firebaseapp.com",
  databaseURL: "https://app-gallery-1ccfd.firebaseio.com",
  projectId: "app-gallery-1ccfd",
  storageBucket: "app-gallery-1ccfd.appspot.com",
  messagingSenderId: "1002929791972",
  appId: "1:1002929791972:web:c45cc8097ef4f42362ddf1",
  measurementId: "G-J3P538F5RV"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
